<<<<<<< HEAD
var app = angular.module("myApp", ['ui.router','media']); //'ui.bootstrap'
app.config(function($stateProvider, $urlRouterProvider) {
    
	$urlRouterProvider.otherwise('/index-login')
	
	$stateProvider
	.state('index-login' ,{
		url: '/index-login',
		templateUrl: './ng-view/index-login.html'
	})
	.state('dashboard' ,{
		url: '/dashboard/:customer_id',
		templateUrl: './ng-view/customer/dashboard.html'
	})
	.state('userregistration' ,{
		url: '/userregistration',
		templateUrl: './ng-view/userregistration.html'
	})
	.state('vetdocregistration' ,{
		url: '/vetdocregistration',
		templateUrl: './ng-view/vetdocregistration.html'
	})
	.state('animal-register' ,{
		url: '/animal-register/:customer_id',
		templateUrl: './ng-view/customer/animal-register.html'
	})
	.state('petview' ,{
		url: '/petview/:customer_id',
		templateUrl: './ng-view/customer/petview.html'
	})
	.state('petmap' ,{
		url: '/petmap/:customer_id',
		templateUrl: './ng-view/customer/map.html'
	})
	.state('petchart' ,{
		url: '/petchart/:customer_id',
		templateUrl: './ng-view/customer/petcharts.html'
	})
	.state('doctordashboard' ,{
		url: '/doctordashboard/:doctorid',
		templateUrl: './ng-view/doctor/docdashboard.html'
	})
	.state('doctorview' ,{
		url: '/doctorview/:customer_id',
		templateUrl: './ng-view/customer/doctorview.html'
	})
	.state('doctor-petchart' ,{
		url: '/doctor-petchart/:doctorid',
		templateUrl: './ng-view/doctor/doctor-pet.html'
	})
	.state('customerdetails' ,{
		url: '/customerdetails/:doctorid',
		templateUrl: './ng-view/doctor/customerdetails.html'
	})
	.state('medicalsupporter' ,{
		url: '/medicalsupporter',
		templateUrl: './ng-view/medicalsupporterreg.html'
	})
	.state('medicaldetails' ,{
		url: '/medicaldetails/:doctorid',
		templateUrl: './ng-view/doctor/medicaldetails.html'
	})
	.state('ecomregistration' ,{
		url: '/ecomregistration',
		templateUrl: './ng-view/ecommerceregistration.html'
	})
	.state('ecomdashboard' ,{
		url: '/ecomdashboard/:sellerid',
		templateUrl: './ng-view/Ecomdashboard.html'
	})
	.state('docappointment' ,{
		url: '/docappointment/:doctorid',
		templateUrl: './ng-view/doctor/appointment.html'
	})
	.state('productform' ,{
		url: '/productform/:sellerid',
		templateUrl: './ng-view/Ecommerce/productinsertform.html'
	})
	.state('productdetails' ,{
		url: '/productdetails/:sellerid',
		templateUrl: './ng-view/Ecommerce/productsdetailspage.html'
	})
	.state('products' ,{
		url: '/products/:sellerid',
		templateUrl: './ng-view/Ecommerce/product.html'
	})
	.state('cattle' ,{
		url: '/cattle/:sellerid',
		templateUrl: './ng-view/Ecommerce/cattle.html',
		controller:'ecomctrl'
	})
	.state('customercattle' ,{
		url: '/customercattle/:customer_id',
		templateUrl: './ng-view/customer/customercattle.html',
		controller:'cusEComCtrl'
	})
	.state('customerproduct' ,{
		url: '/customerproduct/:customer_id',
		templateUrl: './ng-view/customer/customerproduct.html',
		controller:'cusEComCtrl'
   })
   .state('offers' ,{
		url: '/offers/:customer_id',
		templateUrl: './ng-view/customer/offerspage.html',
		controller:'cusEComCtrl'
   })
   .state('appointmentconfirmation' ,{
		url: '/appointmentconfirmation/:customer_id',
		templateUrl: './ng-view/customer/appointmentconfirmation.html',
   })
   .state('meddashboard' ,{
		url: '/meddashboard/:med_id',
		templateUrl: './ng-view/medicalsupplier/meddashboard.html',
   })
   .state('med-doctordetails' ,{
		url: '/med-doctordetails/:med_id',
		templateUrl: './ng-view/medicalsupplier/doctordetails.html',
   })
   .state('med-docappointment' ,{
		url: '/med-docappointment/:med_id',
		templateUrl: './ng-view/medicalsupplier/med-doctorappointment.html',
   })
   .state('medappointmentlist' ,{
		url: '/medappointmentlist/:doctorid',
		templateUrl: './ng-view/doctor/medappointment.html',
   })
   .state('approvedcustomerlist' ,{
		url: '/approvedcustomerlist/:doctorid',
		templateUrl: './ng-view/doctor/approvedviewlist.html',
   })
   .state('approvedmedlist' ,{
		url: '/approvedmedlist/:doctorid',
		templateUrl: './ng-view/doctor/medapprovedviewlist.html',
   })
   .state('petboundarymap' ,{
		url: '/petboundarymap/:customer_id',
		templateUrl: './ng-view/customer/petboundarymap.html',
   })
   .state('medical-event' ,{
		url: '/medical-event/:doctorid',
		templateUrl: './ng-view/doctor/medical-event.html',
=======
var app = angular.module("myApp", ['ui.router','media']);
app.config(function($stateProvider, $urlRouterProvider) {
    
	$urlRouterProvider.otherwise('/index-login')
	
	$stateProvider
	.state('index-login' ,{
		url: '/index-login',
		templateUrl: './ng-view/index-login.html'
	})
	.state('dashboard' ,{
		url: '/dashboard/:customer_id',
		templateUrl: './ng-view/customer/dashboard.html'
	})
	.state('userregistration' ,{
		url: '/userregistration',
		templateUrl: './ng-view/userregistration.html'
	})
	.state('vetdocregistration' ,{
		url: '/vetdocregistration',
		templateUrl: './ng-view/vetdocregistration.html'
	})
	.state('animal-register' ,{
		url: '/animal-register/:customer_id',
		templateUrl: './ng-view/customer/animal-register.html'
	})
	.state('petview' ,{
		url: '/petview/:customer_id',
		templateUrl: './ng-view/customer/petview.html'
	})
	.state('petmap' ,{
		url: '/petmap/:customer_id',
		templateUrl: './ng-view/customer/map.html'
	})
	.state('petchart' ,{
		url: '/petchart/:customer_id',
		templateUrl: './ng-view/customer/petcharts.html'
	})
	.state('doctordashboard' ,{
		url: '/doctordashboard/:doctorid',
		templateUrl: './ng-view/doctor/docdashboard.html'
	})
	.state('doctorview' ,{
		url: '/doctorview/:customer_id',
		templateUrl: './ng-view/customer/doctorview.html'
	})
	.state('doctor-petchart' ,{
		url: '/doctor-petchart/:doctorid',
		templateUrl: './ng-view/doctor/doctor-pet.html'
	})
	.state('customerdetails' ,{
		url: '/customerdetails/:doctorid',
		templateUrl: './ng-view/doctor/customerdetails.html'
	})
	.state('medicalsupporter' ,{
		url: '/medicalsupporter',
		templateUrl: './ng-view/medicalsupporterreg.html'
	})
	.state('medicaldetails' ,{
		url: '/medicaldetails/:doctorid',
		templateUrl: './ng-view/doctor/medicaldetails.html'
	})
	.state('ecomregistration' ,{
		url: '/ecomregistration',
		templateUrl: './ng-view/ecommerceregistration.html'
	})
	.state('ecomdashboard' ,{
		url: '/ecomdashboard/:sellerid',
		templateUrl: './ng-view/Ecomdashboard.html'
	})
	.state('docappointment' ,{
		url: '/docappointment/:doctorid',
		templateUrl: './ng-view/doctor/appointment.html'
	})
	.state('productform' ,{
		url: '/productform/:sellerid',
		templateUrl: './ng-view/Ecommerce/productinsertform.html'
	})
	.state('productdetails' ,{
		url: '/productdetails/:sellerid',
		templateUrl: './ng-view/Ecommerce/productsdetailspage.html'
	})
	.state('products' ,{
		url: '/products/:sellerid',
		templateUrl: './ng-view/Ecommerce/product.html'
	})
	.state('cattle' ,{
		url: '/cattle/:sellerid',
		templateUrl: './ng-view/Ecommerce/cattle.html',
		controller:'ecomctrl'
	})
	.state('customercattle' ,{
		url: '/customercattle/:customer_id',
		templateUrl: './ng-view/customer/customercattle.html',
		controller:'cusEComCtrl'
	})
	.state('customerproduct' ,{
		url: '/customerproduct/:customer_id',
		templateUrl: './ng-view/customer/customerproduct.html',
		controller:'cusEComCtrl'
   })
   .state('offers' ,{
		url: '/offers/:customer_id',
		templateUrl: './ng-view/customer/offerspage.html',
		controller:'cusEComCtrl'
   })
   .state('appointmentconfirmation' ,{
		url: '/appointmentconfirmation/:customer_id',
		templateUrl: './ng-view/customer/appointmentconfirmation.html',
   })
   .state('meddashboard' ,{
		url: '/meddashboard/:med_id',
		templateUrl: './ng-view/medicalsupplier/meddashboard.html',
   })
   .state('med-doctordetails' ,{
		url: '/med-doctordetails/:med_id',
		templateUrl: './ng-view/medicalsupplier/doctordetails.html',
   })
   .state('med-docappointment' ,{
		url: '/med-docappointment/:med_id',
		templateUrl: './ng-view/medicalsupplier/med-doctorappointment.html',
   })
   .state('medappointmentlist' ,{
		url: '/medappointmentlist/:doctorid',
		templateUrl: './ng-view/doctor/medappointment.html',
   })
   .state('approvedcustomerlist' ,{
		url: '/approvedcustomerlist/:doctorid',
		templateUrl: './ng-view/doctor/approvedviewlist.html',
   })
   .state('approvedmedlist' ,{
		url: '/approvedmedlist/:doctorid',
		templateUrl: './ng-view/doctor/medapprovedviewlist.html',
>>>>>>> refs/remotes/origin/master
   })
});