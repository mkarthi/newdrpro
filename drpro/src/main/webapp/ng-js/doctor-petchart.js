app.controller('doccharCtrl', function($scope,$http,$stateParams,$filter,$rootScope) {

	$scope.cid = window.location.hash;
	var customerid =$scope.cid.split("/");
	$scope.cid = customerid[2];
	
	$rootScope.docid = $scope.cid; 
	
    //For all animal chart in doctor side
	/*$http.get("./rest/doctordata/doctorpetchart").then(function(response){
		$scope.doctorpetchart = response.data;
		petcharts();
	});*/
	
	$http.get("./rest/doctordata/appointmentpetchart/"+$rootScope.docid).then(function(response){
		$scope.appointmentpetchart = response.data;
		petcharts();
		$scope.showpetlist = true;
		$scope.viewchart = function(data){
			var petid = data.animalid;
			var drange = data.disease_range;
			$scope.getrange = drange.split(',');
			$scope.range1 =$scope.getrange[0] +"%";
			$scope.range2 =$scope.getrange[1] +"%";
			$scope.range3 =$scope.getrange[2] +"%";
			$scope.range4 =$scope.getrange[3] +"%";
			$scope.showpetchart = true;
		}
	});
	
	function petcharts(){
		   // set the data
		   var data = $scope.appointmentpetchart;
		   // create the chart
		   var chart = anychart.pie();
		   // set the chart title
		   chart.title("");
		   // add the data
		   chart.data(data);
		   // display the chart in the container
		   chart.container('animalpie');
		   chart.draw();
	}
	
	
	//Get for animal list 
	/*$http.get("./rest/doctordata/animallist").then(function(response){
		$scope.getanimallist = response.data;
		$scope.showpetlist = true;
		$scope.viewchart = function(data){
			var petid = data.animalid;
			var drange = data.disease_range;
			$scope.getrange = drange.split(',');
			$scope.range1 =$scope.getrange[0] +"%";
			$scope.range2 =$scope.getrange[1] +"%";
			$scope.range3 =$scope.getrange[2] +"%";
			$scope.range4 =$scope.getrange[3] +"%";
			$scope.showpetchart = true;
		}
    });*/
	
	
	
});