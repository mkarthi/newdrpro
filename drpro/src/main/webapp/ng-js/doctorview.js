app.controller('docviewCtrl', function($scope,$http,$stateParams,$filter,$rootScope,$compile) {

	$scope.cid = window.location.hash;
	var customerid =$scope.cid.split("/");
	var cid = customerid[2];
	
	$scope.tab1view = true;
	$scope.tab1 = function(){
		$scope.tab1view = true;
		$scope.tab2view = false;
		$scope.tab3view = false;
	}
	$scope.tab2 = function(){
		$scope.tab2view = true;
		$scope.tab1view = false;
		$scope.tab3view = false;
	}
	$scope.tab3 = function(){
		$scope.tab3view = true;
		$scope.tab1view = false;
		$scope.tab2view = false;
	}
    
    
	$http.get("./rest/registration/getdoclist/"+cid).then(function(response){
		$scope.getdoclist = response.data;
		
		var datesplit = [];
		var dateview = [];
		$scope.getsplitdate = []
		for(var i=0;i<$scope.getdoclist.length;i++){
			datesplit.push($scope.getdoclist[i].dateandtime)
			dateview.push(datesplit[i]);
			var sdate = dateview[i].split(" ");
			$scope.totdate = sdate[1]+" "+sdate[2]+"-"+sdate[5]+" "+sdate[6];
			$scope.getsplitdate.push($scope.totdate);
		}
		
		
		//getdocmap();
		
		     $scope.MapOptions = {
		        center: new google.maps.LatLng($scope.getdoclist[0].latitude, $scope.getdoclist[0].longitude),
		        zoom: 4,
		        mapTypeId: google.maps.MapTypeId.ROADMAP
		     };
		     
		     var icon = { 
		  		    url: './images/doctor.png'                             
		  	 };
		     
		     //Initializing the InfoWindow, Map and LatLngBounds objects.
		     $scope.InfoWindow = new google.maps.InfoWindow();
		     $scope.Latlngbounds = new google.maps.LatLngBounds();
		     $scope.Map = new google.maps.Map(document.getElementById("docMap"), $scope.MapOptions);
		
		     //Looping through the Array and adding Markers.
		     for (var i = 0; i < $scope.getdoclist.length; i++) {
		         var data = $scope.getdoclist[0];
		         var myLatlng = new google.maps.LatLng(data.latitude, data.longitude);
		
		         //Initializing the Marker object.
		         var marker = new google.maps.Marker({
		             position: myLatlng,
		             map: $scope.Map,
		             doctor_name: data.doctor_name,
		             icon:icon
		         });
		         
		         //Adding InfoWindow to the Marker.
		         (function (marker, data) {
		             google.maps.event.addListener(marker, "click", function (e) {
		            	 $scope.getdocdata=data;
		            	 $scope.docname = data.doctor_name;
		            	 $scope.docemail = data.doctor_email;
		            	 $scope.docphno = data.doctor_phoneno;
		            	 $scope.docloc = data.location;
		            	 
		                 $scope.InfoWindow.setContent("<div>" + data.doctor_name + ',' + data.latitude + ',' + data.longitude + "</div>");
		                 $scope.InfoWindow.open($scope.Map, marker);
		             });
		         })(marker, data);
		
		         //Plotting the Marker on the Map.
		         $scope.Latlngbounds.extend(marker.position);
		     }
		
		     //Adjusting the Map for best display.
		     $scope.Map.setCenter($scope.Latlngbounds.getCenter());
		     $scope.Map.fitBounds($scope.Latlngbounds);
		
	});
	
	//get animal list of customer for doctor appointment ......
	$http.get("./rest/registration/getanimallist/"+cid).then(function(response){
		$scope.getanimallist = response.data;
		$scope.uniques = _.map(_.groupBy($scope.getanimallist,function(doc){
			  return doc.animal_category;
			}),function(grouped){
			  return grouped[0];
		});
	});
	
	
	
	$scope.cusid = cid 
	$scope.confirmappoint = function(data){
		$scope.getanimal = function(animalcategory){
			$scope.eeee = animalcategory;
			$http.get("./rest/registration/getindividualpet/"+animalcategory+"/"+cid).then(function(response){
				$scope.getindividuallist = response.data;
				$scope.getanimalname = function(did){
					$scope.getpetname= _.where($scope.getindividuallist, {animalid:did});
					$scope.getaname = $scope.getpetname[0].animal_name; 
					$scope.getaddress = $scope.getpetname[0].address;
					$scope.getage = $scope.getpetname[0].age;
					$scope.deviceid = $scope.getpetname[0].uid;
					$scope.disesestatus = $scope.getpetname[0].disease_range;
					$scope.animalid = $scope.getpetname[0].animalid;
				}
				
			});
		}
		$scope.docid=data.doctorid;
		$scope.request = "Pending";
		$scope.type="Customer";
		//updateappointment(data);
	}
	
	$scope.wasSubmitted = false;
	$scope.appointmentclk = function(){
		
		$scope.wasSubmitted = true;
			var dates = $('#datetimepicker1').val();
			$scope.getdate = dates;
			var dateval = $scope.getdate.split("/");
			
			$scope.aligndate = dateval[1]+"/"+dateval[0]+"/"+dateval[2];
			
			var data ={
					 appointmentid:0,
	        		 customer_id:$scope.cusid,
	          		 doctorid:$scope.docid,
	          		 request:$scope.request,
	          		 type:$scope.type,
	          		 animalid:$scope.animalid,
	          		 dateandtime:$scope.aligndate,
	          		// pet_image:$scope.photo
	          	}
			
			updateimg(data);
			
		
	}
	
	/*$scope.appointmentclk = function(){
		var dates = $('#datetimepicker1').val();
		$scope.getdate = dates;
		$.confirm({
		    title: 'Send Request!',
		    content: 'For Appointment!',
		    buttons: {
		        confirm: function () {
		        	var data ={
		           		 appointmentid:0,
		           		 customer_id:$scope.cusid,
		           		 doctorid:$scope.docid,
		           		 request:$scope.request,
		           		 type:$scope.type,
		           		 animalid:$scope.animalid,
		           		 dateandtime:$scope.getdate,
		           		 pet_image:$scope.photo
		           	}
		           	
		        	updateimg(data);
		        	
		        	     	
		        	$.alert('Confirmed!  You can check My Veterinarian tab');
		        },
		        cancel: function () {
		            $.alert('Canceled!');
		        },
		    }
		});
	}*/    
	
	$scope.sendappointment = function(data){
		$http.post("./rest/registration/userrequest",data).then(function(response){
				$scope.userappointment = response.data;
				alert("Sent Request Successfully");
				window.location.reload();
		});
	}
	
	
	var updateimg = function(data){
		var files = angular.element('.imguploads');
		if(files.val() != 0){
			var formdata = new FormData();
		 	for(var i=0;i<files.length;i++){ 
		 		var fileObj= files[i].files;
		 		formdata.append("files" ,fileObj[0]);   
		    }
		 	
		 	 var xhr = new XMLHttpRequest();    
		 	 xhr.open("POST","./rest/file/upload/drproimg");
		 	xhr.send(formdata);
		    	xhr.onload = function(e) {
		    		if (this.status == 200) {
		    			//$scope.tmppath;
		    			var obj = JSON.parse(this.responseText);
		    			
		    			var img="";	
		    			
		    			for(i=0;i<obj.length;i++){
	 	    				var path = obj[i].path;
	 	    				img += path+","; 
	 	    			} 
		    			
		    			data.pet_images = img;
		    			
		    			if(data.appointmentid == 0){
		    				if($scope.files!=0){
		    					
		    				 var fd = new FormData();
		    				     //Take the first selected file
		    		    	 for (var i in $scope.files) {
		    		    		 fd.append("file", $scope.files[i]);
		    		         }
		    				 fd.append("file", $scope.files[i]);
		    				 
		    				 
		    				 var xhr = new XMLHttpRequest();
		    				 xhr.open("POST","./rest/video/videoupload/drprovideo");
		    				 xhr.send(fd);
		    			    	xhr.onload = function(e) {
		    			    		if (this.status == 200) {
		    			    			var vobj = JSON.parse(this.responseText);
		    			    			
		    			    			var img="";	
		    			    			
		    			    			var videopath = vobj[0].path; 
		    			    			
		    			    			data.pet_videos = videopath;
		    			    			
		    			    			$scope.uploadstatus=vobj;
			    				    	
			    				    	 if(data.appointmentid == 0){
			    				    		  alert("File Successfully upload");
			    				    		  $scope.sendappointment(data);	
			    				    	  }else{
			    				    		  $scope.sendappointment(data);	
			    				    		  alert("Error");
			    				    	  }
			    				    
		    			    		}
		    			    	}
		    				 	
		    		         
		    				}else{
		    				//	$scope.sendappointment(data);	
		    				}
		    				
		    			}else{
		    				
		    			}
	    		    }
		    	};
		}else{
			alert("Process failiure :)")
			$scope.sendappointment(data);
		}
	};
	
	
<<<<<<< HEAD
    //Event List
	$http.get("./rest/doctordata/eventlist").then(function(response){
		$scope.geteventlist = response.data;
		
		for(var i=0;i<$scope.geteventlist.length;i++){
			 $scope.geteventlist[i].date;
		}
	});
	
	//Mutiple image upload and view 
	
	$scope.addimg = function(){
		 var add = angular.element( document.querySelector( '#addfield' ) );
		 add.append('<div class="form-group">'+
						'<div class="image-editor multiimg">'+  
						'<input type="file"  id="imgupload" ng-model="imgupload" pet_images="{{pet_images}}" name="file[]"  style="padding-left: 0px;width:100%" class="imguploads col-md-4 cropit-image-input" />'+
						'<i style="width: 0px;" class="glyphicon glyphicon-trash removebtn" ng-click="remove()"></i>'+
						'</div>'+	
					'</div>');
		 
		 $('.removebtn').click(function(){
				$(this).parent('.multiimg').parent('.form-group').hide();
//				$(this).parent().hide();
			});
	};
	
	
	/*$('#i_file').change( function(event) {
		$scope.tmppath = URL.createObjectURL(event.target.files[0]);
	    $("#viewimg").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));
		    
	    $("#disp_tmp_path").html("Temporary Path(Copy it and try pasting it in browser address bar) --> <strong>["+$scope.tmppath+"]</strong>");
   
	    $.ajax({
			url : 'GetUserServlet',
			data : {
				tmppath : $scope.tmppath
			},
			success : function(responseText) {
				console.log("responseText" ,+ responseText );
			}
		});

	});*/
	
	
	$http.get("./rest/registration/appointconfirmed/"+cid).then(function(response){
		$scope.getconfirmation = response.data;
	});
	
	
	//Doctor list and location in medical dashboard appointment page
	$scope.getdoclocation = function(data){
		
		   $http.get("./rest/medsupport/getdoclocation/"+data.doctorid).then(function(response){
			   $scope.getindividualdoc = response.data;
				
			$scope.medaddress = $scope.getindividualdoc[0].location; 
			   
			var geocoder = new google.maps.Geocoder();
			var address = $scope.medaddress;

			geocoder.geocode( { 'address': address}, function(results, status) {

			if (status == google.maps.GeocoderStatus.OK) {
				var latitude = results[0].geometry.location.lat();
				var longitude = results[0].geometry.location.lng();
		    } 
			

		     $scope.MapOptions = {
		         center: new google.maps.LatLng(9.925201, 78.119774),
		         zoom: 15,
		         mapTypeId: google.maps.MapTypeId.ROADMAP
		     };
		     
		     var icon = { 
		  		    url: './images/doctor.png'                             
		  	 };
		     
		     //Initializing the InfoWindow, Map and LatLngBounds objects.
		     $scope.InfoWindow = new google.maps.InfoWindow();
		     $scope.Latlngbounds = new google.maps.LatLngBounds();
		     $scope.Map = new google.maps.Map(document.getElementById("docMap"), $scope.MapOptions);

		     //Looping through the Array and adding Markers.
		     for (var i = 0; i < $scope.getindividualdoc.length; i++) {
		         var data = $scope.getindividualdoc[i];
		         var myLatlng = new google.maps.LatLng(latitude, longitude);

		         //Initializing the Marker object.
		         var marker = new google.maps.Marker({
		             position: myLatlng,
		             map: $scope.Map,
		             location: data.location,
		             icon:icon
		         });

		         //Adding InfoWindow to the Marker.
		         (function (marker, data) {
		             google.maps.event.addListener(marker, "click", function (e) {
		                 $scope.InfoWindow.setContent("<div>" + data.doctor_name + ',' + data.location + ',' + data.dateandtime +"</div>");
		                 $scope.InfoWindow.open($scope.Map, marker);
		             });
		         })(marker, data);

		         //Plotting the Marker on the Map.
		         $scope.Latlngbounds.extend(marker.position);
		     }

		     //Adjusting the Map for best display.
		     $scope.Map.setCenter($scope.Latlngbounds.getCenter());
		     $scope.Map.fitBounds($scope.Latlngbounds);
			});
			
			});
		   }
	
	
	$scope.getFileDetails = function (e) {

        $scope.files = [];
        var fileInput = e.files[0];
	     var filePath = fileInput.name;
	    var allowedExtensions = /(\.mp4|\.flv|\.mov|\.3gp)$/i;
	    if(!allowedExtensions.exec(filePath)){
	    	$(".alert-danger").show(); 
	    	/*alert('Please upload file having extensions .mp4/.flv/.mov/.3gp only.');*/
	        fileInput.value = '';
	        return false;
	    }else{
	    	$(".alert-danger").hide(); 
	       $scope.$apply(function () {
	            // STORE THE FILE OBJECT IN AN ARRAY.
	              for (var i = 0; i < e.files.length; i++) {
	                  $scope.files.push(e.files[i])
	              }
	          });
	    }
	 }; 
	
	//date function event(click)
	 $scope.dateclk = function(){
		 $(function () {
		    $('#datetimepicker1').datetimepicker();
		 });
	 }
=======
	//Mutiple image upload and view 
	
	$scope.addimg = function(){
		 var add = angular.element( document.querySelector( '#addfield' ) );
		 add.append('<div class="form-group">'+
						'<div class="image-editor multiimg">'+  
						'<input type="file"  id="imgupload" ng-model="imgupload" pet_images="{{pet_images}}" name="file[]"  style="padding-left: 0px;width:100%" class="imguploads col-md-4 cropit-image-input" />'+
						'<i style="width: 0px;" class="glyphicon glyphicon-trash removebtn" ng-click="remove()"></i>'+
						'</div>'+	
					'</div>');
		 
		 $('.removebtn').click(function(){
				$(this).parent('.multiimg').parent('.form-group').hide();
//				$(this).parent().hide();
			});
	};
	
	
	/*$('#i_file').change( function(event) {
		$scope.tmppath = URL.createObjectURL(event.target.files[0]);
	    $("#viewimg").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));
		    
	    $("#disp_tmp_path").html("Temporary Path(Copy it and try pasting it in browser address bar) --> <strong>["+$scope.tmppath+"]</strong>");
   
	    $.ajax({
			url : 'GetUserServlet',
			data : {
				tmppath : $scope.tmppath
			},
			success : function(responseText) {
				console.log("responseText" ,+ responseText );
			}
		});

	});*/
	
	
	$http.get("./rest/registration/appointconfirmed/"+cid).then(function(response){
		$scope.getconfirmation = response.data;
	});
	
	
	//Doctor list and location in medical dashboard appointment page
	$scope.getdoclocation = function(data){
		
		   $http.get("./rest/medsupport/getdoclocation/"+data.doctorid).then(function(response){
			   $scope.getindividualdoc = response.data;
				
			$scope.medaddress = $scope.getindividualdoc[0].location; 
			   
			var geocoder = new google.maps.Geocoder();
			var address = $scope.medaddress;

			geocoder.geocode( { 'address': address}, function(results, status) {

			if (status == google.maps.GeocoderStatus.OK) {
				var latitude = results[0].geometry.location.lat();
				var longitude = results[0].geometry.location.lng();
		    } 
			

		     $scope.MapOptions = {
		         center: new google.maps.LatLng(9.925201, 78.119774),
		         zoom: 15,
		         mapTypeId: google.maps.MapTypeId.ROADMAP
		     };
		     
		     var icon = { 
		  		    url: './images/doctor.png'                             
		  	 };
		     
		     //Initializing the InfoWindow, Map and LatLngBounds objects.
		     $scope.InfoWindow = new google.maps.InfoWindow();
		     $scope.Latlngbounds = new google.maps.LatLngBounds();
		     $scope.Map = new google.maps.Map(document.getElementById("docMap"), $scope.MapOptions);

		     //Looping through the Array and adding Markers.
		     for (var i = 0; i < $scope.getindividualdoc.length; i++) {
		         var data = $scope.getindividualdoc[i];
		         var myLatlng = new google.maps.LatLng(latitude, longitude);

		         //Initializing the Marker object.
		         var marker = new google.maps.Marker({
		             position: myLatlng,
		             map: $scope.Map,
		             location: data.location,
		             icon:icon
		         });

		         //Adding InfoWindow to the Marker.
		         (function (marker, data) {
		             google.maps.event.addListener(marker, "click", function (e) {
		                 $scope.InfoWindow.setContent("<div>" + data.doctor_name + ',' + data.location + ',' + data.dateandtime +"</div>");
		                 $scope.InfoWindow.open($scope.Map, marker);
		             });
		         })(marker, data);

		         //Plotting the Marker on the Map.
		         $scope.Latlngbounds.extend(marker.position);
		     }

		     //Adjusting the Map for best display.
		     $scope.Map.setCenter($scope.Latlngbounds.getCenter());
		     $scope.Map.fitBounds($scope.Latlngbounds);
			});
			
			});
		   }
	
	
	$scope.getFileDetails = function (e) {

        $scope.files = [];
        var fileInput = e.files[0];
	     var filePath = fileInput.name;
	    var allowedExtensions = /(\.mp4|\.flv|\.mov|\.3gp)$/i;
	    if(!allowedExtensions.exec(filePath)){
	    	$(".alert-danger").show(); 
	    	/*alert('Please upload file having extensions .mp4/.flv/.mov/.3gp only.');*/
	        fileInput.value = '';
	        return false;
	    }else{
	    	$(".alert-danger").hide(); 
	       $scope.$apply(function () {
	            // STORE THE FILE OBJECT IN AN ARRAY.
	              for (var i = 0; i < e.files.length; i++) {
	                  $scope.files.push(e.files[i])
	              }
	          });
	    }
	 }; 
	
>>>>>>> refs/remotes/origin/master
	 
	/* $scope.uploadFiles = function () {
         
    	 var fd = new FormData();
		     //Take the first selected file
    	 for (var i in $scope.files) {
    		 fd.append("file", $scope.files[i]);
         }
		 fd.append("file", $scope.files[i]);
         $http.post('./rest/video/videoupload', fd, {
		        withCredentials: true,
		        headers: {'Content-Type': undefined },
		        transformRequest: angular.identity
		    }).then(function(response){
		    	  $scope.uploadstatus=response;
		    	  if($scope.uploadstatus=="OK"){
		    		  alert("Error");
		    	  }else{
		    		  alert("File Successfully upload");
		    	  }
		    });
      }*/
	
});