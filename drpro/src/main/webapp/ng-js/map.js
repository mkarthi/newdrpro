app.controller('mapCtrl', function($scope,$http,$stateParams,$filter,$rootScope,$compile) {
	
	
	 $scope.cid = window.location.hash;
	 var customerid =$scope.cid.split("/");
	 var cid = customerid[2];
	   
	 $http.get("./rest/registration/getanimallist/"+cid).then(function(response){
			$scope.getanimallist = response.data;
			getpetmap();
			//getusermap();
	 });
	
	/* $scope.Markers = [
         {
             "title": 'Madurai',
             "lat": '9.920130',
             "lng": '78.110878',
         },
         {
             "title": 'Apk',
             "lat": '9.882960',
             "lng": '78.112540',
         },
     ];*/

     //Setting the Map options.
	 
	 
	 
	 function getpetmap(){
		angular.forEach($scope.getanimallist, function(value, key){
			
			 $scope.petlocation = value.address
			
			 var geocoder = new google.maps.Geocoder();
			 var address = $scope.petlocation; 
			 var results = $scope.getanimallist;
			   
			 geocoder.geocode( { 'address': address}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					for(var i =0;i<results.length;i++){
						var latitude = results[i].geometry.location.lat();
						var longitude = results[i].geometry.location.lng();
					}
			    } 
			
				
		     $scope.MapOptions = {
		         center: new google.maps.LatLng(latitude, longitude),
		         zoom: 4,
		         mapTypeId: google.maps.MapTypeId.ROADMAP
		     };
		     
		     var icon = { 
		  		    url: './images/cow.png'                             
		  	 };
		     
		     //Initializing the InfoWindow, Map and LatLngBounds objects.
		     $scope.InfoWindow = new google.maps.InfoWindow();
		     $scope.Latlngbounds = new google.maps.LatLngBounds();
		     $scope.Map = new google.maps.Map(document.getElementById("dvMap"), $scope.MapOptions);
		
		     //Looping through the Array and adding Markers.
		     for (var i = 0; i < $scope.getanimallist.length; i++) {
		         var data = $scope.getanimallist[i];
		         var myLatlng = new google.maps.LatLng(data.latitude,data.longitude);
		
		         //Initializing the Marker object.
		         var marker = new google.maps.Marker({
		             position: myLatlng,
		             map: $scope.Map,
		             animal_category: data.animal_category,
		             icon:icon
		         });
		
		         //Adding InfoWindow to the Marker.
		         (function (marker, data) {
		             google.maps.event.addListener(marker, "click", function (e) {
		                 $scope.InfoWindow.setContent("<div>" + data.animal_category + ',' + data.animal_name + ',' + data.age + "</div>");
		                 $scope.InfoWindow.open($scope.Map, marker);
		             });
		         })(marker, data);
		
		         //Plotting the Marker on the Map.
		         $scope.Latlngbounds.extend(marker.position);
		     }
		
		     //Adjusting the Map for best display.
		     $scope.Map.setCenter($scope.Latlngbounds.getCenter());
		     $scope.Map.fitBounds($scope.Latlngbounds);
		     
			 });
		 });
	 }
<<<<<<< HEAD
	 
	 
	 
	 //Pet boundary map
	 
	 $http.get("./rest/registration/getanimallist/"+cid).then(function(response){
		$scope.getanimallist = response.data;
		$scope.uniques = _.map(_.groupBy($scope.getanimallist,function(doc){
			  return doc.animal_category;
		}),function(grouped){
			  return grouped[0];
		});
	});
	 
	 $scope.getanimal = function(animalcategory){
		$scope.eeee = animalcategory;
		$http.get("./rest/registration/getindividualpet/"+animalcategory+"/"+cid).then(function(response){
			$scope.getindividuallist = response.data;
			$scope.getanimalname = function(did){
				$scope.getpetname= _.where($scope.getindividuallist, {animalid:did});
				boundarymap();
				$scope.getmeter = function(){
					$scope.getkm;
				}
			}
		});
	 }
	 
	 $http.get("./rest/registration/getanimallist/"+cid).then(function(response){
			$scope.getanimallist = response.data;
			$scope.petcount = $scope.getanimallist.length;  
	 });
	 
	 function boundarymap(){
			angular.forEach($scope.getanimallist, function(value, key){
				
				 $scope.petlocation = value.address
				
				 var geocoder = new google.maps.Geocoder();
				 var address = $scope.petlocation; 
				 var getpetlocation = $scope.getanimallist;
				 var results= _.pluck(getpetlocation,'address');
				   
				 geocoder.geocode( { 'address': address}, function(results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						for(var i =0;i<results.length;i++){
							var latitude = results[i].geometry.location.lat();
							var longitude = results[i].geometry.location.lng();
										
					     $scope.MapOptions = {
					         center: new google.maps.LatLng(latitude, longitude),
					         zoom: 10,
					         mapTypeId: google.maps.MapTypeId.ROADMAP
					     };
					  }
					} 
				  });
				 
				     var icon = { 
				  		    url: './images/cow.png'                             
				  	 };
				     
				     //Initializing the InfoWindow, Map and LatLngBounds objects.
				     $scope.InfoWindow = new google.maps.InfoWindow();
				     $scope.Latlngbounds = new google.maps.LatLngBounds();
				     $scope.Map = new google.maps.Map(document.getElementById("boundarymap"), $scope.MapOptions);
    			
				     var marker = new google.maps.Marker({
					      position: {lat: 9.91966, lng: 78.11939},
					      map: $scope.Map,          
					      draggable: true,
					      title: 'Animal marker'
					  });
					  
					  marker.addListener('dragend', function(event){
					      var a=cityCircle.getBounds();
					      if(!a.contains(event.latLng))
					      alert("Your Animal Going Outside From Boundry Range");                
					  });
					         
					   var cityCircle ="";

					  google.maps.event.addListener($scope.Map, 'click', function(event){
					       cityCircle = new google.maps.Circle({
						     strokeColor: '#FF0000',
						     strokeOpacity: 0.8,
						     strokeWeight: 2,
						     fillColor: '#ffffff',
						     fillOpacity: 0.1,
						     map: $scope.Map,
						     center: event.latLng,
						     radius: 1000 * $scope.getkm,
						     draggable: true,
						     geodesic: true,
						     editable: true     
					       });
					       marker.setPosition(event.latLng);
					    });

					   google.maps.event.addListener($scope.Map, 'click', function(event){                
						     var oldLatLng = marker.getPosition();         
						     var a=cityCircle.getBounds();
						     marker.setPosition(event.latLng);
						     if(!a.contains(event.latLng))
						     alert("Your Animal Going Outside From Boundry Range");                                
					    });
					   
					   document.getElementById('save_boundary').addEventListener('click', function(){
						  var center=cityCircle.getCenter();
						  var radius=parseInt(cityCircle.getRadius());
						  alert("Center latitude of Boundary :"+center.lat());
						  alert("Center longitude of Boundary :"+center.lng());
						  alert("Radius Value of Boundary  :"+radius);
						  $scope.latitude = center.lat();
						  $scope.longitude = center.lng();
						  $scope.radius = radius;
						  
						  var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude );
						  geocoder.geocode({
						      latLng: latLng
						    },
						    function(responses) {
						    if (responses && responses.length > 0)
						    {
						       $scope.address = responses[0].formatted_address;
						    } else {
						      alert('Cannot determine address at this location.');
						    }
						  });

						  
						  
						  var data = {
								 pet_boundariesid:0,
								 latitude:$scope.latitude,
								 longitude:$scope.longitude,
								 boundary_radius:$scope.radius,
								 address:$scope.address
						  }
						  
						  $http.post("./rest/registration/petboundary",data).then(function(response){
							 $scope.saveboundaries = response.data; 
						  });
						  
					  });
					   
					$scope.removeLine = function(){
						alert("ok");
						cityCircle.setMap($scope.Map, null);
					}
					   
					  
			     //Looping through the Array and adding Markers.
			     for (var i = 0; i < $scope.getanimallist.length; i++) {
			    	 var data = $scope.getanimallist[i];
			         var myLatlng = new google.maps.LatLng(data.latitude,data.longitude);
			
			         //Initializing the Marker object.
			         var markers = new google.maps.Marker({
			             position: myLatlng,
			             map: $scope.Map,
			             animal_category: data.animal_category,
			             icon:icon
			         });
			
			         //Adding InfoWindow to the Marker.
			         (function (markers, data) {	
			             google.maps.event.addListener(marker, "click", function (e) {
			                 $scope.InfoWindow.setContent("<div>" + data.animal_category + ',' + data.animal_name + ',' + data.age + "</div>");
			                 $scope.InfoWindow.open($scope.Map, markers);
			             });
			         })(markers, data);
			
			         //Plotting the Marker on the Map.
			         $scope.Latlngbounds.extend(markers.position);
			     }
			
			     //Adjusting the Map for best display.
			     $scope.Map.setCenter($scope.Latlngbounds.getCenter());
			     $scope.Map.fitBounds($scope.Latlngbounds);
				 
			 });
			
			
	     	     
		 }

	 
	 /*function getbound(){
		 angular.forEach($scope.getanimallist, function(value, key){
		 $scope.petlocation = value.address
			
		 var geocoder = new google.maps.Geocoder();
		 var address = $scope.petlocation; 
		 var getpetlocation = $scope.getanimallist;
		 var results= _.pluck(getpetlocation,'address');
		   
		 geocoder.geocode( { 'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				for(var i =0;i<results.length;i++){
					var latitude = results[i].geometry.location.lat();
					var longitude = results[i].geometry.location.lng();
								
			     $scope.MapOptions = {
			         center: new google.maps.LatLng(latitude, longitude),
			         zoom: 4,
			         mapTypeId: google.maps.MapTypeId.ROADMAP
			     };
				}
		    } 
		 });
	     var icon = { 
	  		    url: './images/cow.png'                             
	  	 };
	     
		 
		 var map = new google.maps.Map(document.getElementById('boundarymap'), {
	           zoom: 18,
	           center: {lat: 9.91966, lng: 78.11939}
	         });
		 
	         var marker = new google.maps.Marker({
	           position: {lat: 9.91966, lng: 78.11939},
	           map: map,          
	           draggable: true,
	           title: 'Animal marker'
	         });
	         
	         marker.addListener('dragend', function(event) 
	           {
	             var a=cityCircle.getBounds();
	             if(!a.contains(event.latLng))
	             alert("Your Animal Going Outside From Boundry Range");                
	           });
	         
	         var cityCircle ="";

	         google.maps.event.addListener(map, 'click', function(event) 
	          {
	        	 cityCircle = new google.maps.Circle({
		             strokeColor: '#FF0000',
		             strokeOpacity: 0.8,
		             strokeWeight: 2,
		             fillColor: '#ffffff',
		             fillOpacity: 0.1,
		             map: map,
		             center: event.latLng,
		             radius: 1000 * $scope.getkm ,
		             draggable: true,
		             geodesic: true,
		             editable: true     
		          });
	        	 
	        	 marker.setPosition(event.latLng);
	        	 
	          });

	         google.maps.event.addListener(map, 'click', function(event) 
	         {                
		         var oldLatLng = marker.getPosition();         
		         var a=cityCircle.getBounds();
		         marker.setPosition(event.latLng);
		         if(!a.contains(event.latLng))
		         alert("Your Animal Going Outside From Boundry Range");                                
	         });
	         
	         document.getElementById('save_boundary').addEventListener('click', function() 
	         {
	        	var center=cityCircle.getCenter();
	        	var radius=parseInt(cityCircle.getRadius());
	        	//var category = document.getElementById('category').value;
	        	alert("Center latitude of Boundary :"+center.lat);
	        	alert("Center longitude of Boundary :"+center.lng);
	        	alert("Radius Value of Boundary  :"+radius);           
	          });
	         
	        // boundarymap();
		 });
	         
	 }*/
	 
	 
	  
     
     /*function boundarymap() {        
         var map = new google.maps.Map(document.getElementById('boundarymap'), {
           zoom: 18,
           center: {lat: 9.91966, lng: 78.11939}
         });

         var marker = new google.maps.Marker({
           position: {lat: 9.91966, lng: 78.11939},
           map: map,          
           draggable: true,
           title: 'Animal marker'
         });
         
         marker.addListener('dragend', function(event) 
           {
             var a=cityCircle.getBounds();
             if(!a.contains(event.latLng))
             alert("Your Animal Going Outside From Boundry Range");                
           });
         
         var cityCircle = new google.maps.Circle({
             strokeColor: '#FF0000',
             strokeOpacity: 0.8,
             strokeWeight: 2,
             fillColor: '#ffffff',
             fillOpacity: 0.1,
             map: map,
             center: {lat: 9.925201, lng: 78.119774},
             radius: 1000 * 2 ,
             draggable: true,
             geodesic: true,
             editable: true     
          });

           cityCircle.addListener('click', function(event) 
           {
             //var oldLatLng = marker.getPosition();         
             //var a=cityCircle.getBounds();               
             marker.setPosition(event.latLng);
             //if(!a.contains(event.latLng))
             //alert(a.contains(event.latLng));              
           });

         google.maps.event.addListener(map, 'click', function(event) 
         {                
	         var oldLatLng = marker.getPosition();         
	         var a=cityCircle.getBounds();
	         marker.setPosition(event.latLng);
	         if(!a.contains(event.latLng))
	         alert("Your Animal Going Outside From Boundry Range");                                
         });

         document.getElementById('save_boundary').addEventListener('click', function() 
         {
	            var center=cityCircle.getCenter();
	            var radius=parseInt(cityCircle.getRadius());
	           // var category = document.getElementById('category').value;
	            alert("Selected  Animal  Category :"+category);        
	            alert("Center Location of "+category+" Boundary :"+center);                
	            alert("Radius Value of "+category+" Boundary  :"+radius);           
         });

         document.getElementById('category').addEventListener('change', function() 
         {
           //cityCircle.setCenter({lat: 9.925201, lng: 78.119774});
         });

       }*/
	 
	 
=======
>>>>>>> refs/remotes/origin/master
});