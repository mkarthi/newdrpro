app.controller('medCtrl', function($scope,$http,$stateParams,$rootScope,$filter) {
	
	$scope.meid = window.location.hash;
	var medicalid =$scope.meid.split("/");
	var mid = medicalid[2];
	$scope.mid = mid;
	 
	$rootScope.medid = $scope.mid; 
	   
	$scope.medreg = function(){
		$scope.typeofreg = "medical";
	}
	
	$scope.savemedreg = function(isvalid) {
	   $scope.medreg = function(){
			$scope.typeofreg = "medical";
	   }
	  if(isvalid){
		var data = {
				med_id:0,
				med_name:$scope.supportername, 
				med_address:$scope.street,
				med_ph:$scope.refphoneno,
				med_email:$scope.refemail,
				med_pwd:$scope.pwd,
				typeofreg:$scope.typeofreg
		}
		
		$http.post("./rest/medsupport/medregister",data).then(function(response){
			$scope.savemeddata = response.data;
			alert("Inserted Succesfully");
			window.location.href="#/index-login";
		});
	  }
	}
	
	$http.get("./rest/medsupport/getindividualmed/"+mid).then(function(response){
		$scope.getmeddata = response.data;
		medlocation();
	});
	
	$http.get("./rest/medsupport/getdoclist").then(function(response){
		$scope.getdocdata = response.data;
	});
	
	$scope.prevlogin = function() {
		window.location.href="#/index-login";
	}
	
	$http.get("./rest/doctordata/getmedical").then(function(response){
		$scope.getmed = response.data;
	});
	
	
	$scope.medconfirmappoint = function(data){
		$scope.docid = data.doctorid;
		$scope.doctname = data.doctor_name;
		$scope.medid = $rootScope.medid;
		$scope.type= "Medical Supplier";
		$scope.request = "Pending"; 
	}
	
	
	//For Doctor Appointment
	$scope.medappointmentclk = function(){
		var dates = $('#datetimepicker1').val();
		$scope.getdate = dates;
		$.confirm({
		    title: 'Send Request!',
		    content: 'For Appointment!',
		    buttons: {
		        confirm: function () {
		        	var data ={
		           		 appointmentid:0,
		           		 customer_id:$scope.cusid,
		           		 doctorid:$scope.docid,
		           		 request:$scope.request,
		           		 type:$scope.type,
		           		 animalid:$scope.animalid,
		           		 dateandtime:$scope.getdate,
		           		 med_id:$scope.medid
		           	}
		           	$http.post("./rest/registration/userrequest",data).then(function(response){
		       			$scope.userappointment = response.data;
		       			alert("Sent Request Successfully");
		       			window.location.reload();
		       		});
		        	
		        	 $.alert('Confirmed!  You can check My Veterinarian tab');
		        },
		        cancel: function () {
		            $.alert('Canceled!');
		        },
		    }
		});
	}    
	
	
	
	
	//For individual med-supporter location
	function medlocation (){
		
		$scope.medaddress = $scope.getmeddata[0].med_address; 
		var geocoder = new google.maps.Geocoder();
		var address = $scope.medaddress;

		geocoder.geocode( { 'address': address}, function(results, status) {

		if (status == google.maps.GeocoderStatus.OK) {
			var latitude = results[0].geometry.location.lat();
			var longitude = results[0].geometry.location.lng();
		 } 

	     $scope.MapOptions = {
	         center: new google.maps.LatLng(latitude, longitude),
	         zoom: 15,
	         mapTypeId: google.maps.MapTypeId.ROADMAP
	     };
	     
	     var icon = { 
	  		    url: './images/doctor.png'                             
	  	 };
	     
	     //Initializing the InfoWindow, Map and LatLngBounds objects.
	     $scope.InfoWindow = new google.maps.InfoWindow();
	     $scope.Latlngbounds = new google.maps.LatLngBounds();
	     $scope.Map = new google.maps.Map(document.getElementById("medicalMap"), $scope.MapOptions);

	     //Looping through the Array and adding Markers.
	     for (var i = 0; i < $scope.getmeddata.length; i++) {
	         var data = $scope.getmeddata[i];
	         var myLatlng = new google.maps.LatLng(latitude, longitude);

	         //Initializing the Marker object.
	         var marker = new google.maps.Marker({
	             position: myLatlng,
	             map: $scope.Map,
	             med_address: data.med_address,
	             icon:icon
	         });

	         //Adding InfoWindow to the Marker.
	         (function (marker, data) {
	             google.maps.event.addListener(marker, "click", function (e) {
	                 $scope.InfoWindow.setContent("<div>" + data.med_name + ',' + data.med_address + ',' + data.med_ph+ "</div>");
	                 $scope.InfoWindow.open($scope.Map, marker);
	             });
	         })(marker, data);

	         //Plotting the Marker on the Map.
	         $scope.Latlngbounds.extend(marker.position);
	     }

	     //Adjusting the Map for best display.
	     $scope.Map.setCenter($scope.Latlngbounds.getCenter());
	     $scope.Map.fitBounds($scope.Latlngbounds);
		});
	}
	
	
	//For Doctor location in medical supplier dashboard 
	$scope.getdoclocation = function(data){
		
	   $http.get("./rest/medsupport/getdoclocation/"+data.doctorid).then(function(response){
		   $scope.getindividualdoc = response.data;
			
		$scope.medaddress = $scope.getindividualdoc[0].location; 
		   
		var geocoder = new google.maps.Geocoder();
		var address = $scope.medaddress;

		geocoder.geocode( { 'address': address}, function(results, status) {

		if (status == google.maps.GeocoderStatus.OK) {
			var latitude = results[0].geometry.location.lat();
			var longitude = results[0].geometry.location.lng();
	    } 
		

	     $scope.MapOptions = {
	         center: new google.maps.LatLng(9.925201, 78.119774),
	         zoom: 15,
	         mapTypeId: google.maps.MapTypeId.ROADMAP
	     };
	     
	     var icon = { 
	  		    url: './images/doctor.png'                             
	  	 };
	     
	     //Initializing the InfoWindow, Map and LatLngBounds objects.
	     $scope.InfoWindow = new google.maps.InfoWindow();
	     $scope.Latlngbounds = new google.maps.LatLngBounds();
	     $scope.Map = new google.maps.Map(document.getElementById("docMap"), $scope.MapOptions);

	     //Looping through the Array and adding Markers.
	     for (var i = 0; i < $scope.getindividualdoc.length; i++) {
	         var data = $scope.getindividualdoc[i];
	         var myLatlng = new google.maps.LatLng(latitude, longitude);

	         //Initializing the Marker object.
	         var marker = new google.maps.Marker({
	             position: myLatlng,
	             map: $scope.Map,
	             location: data.location,
	             icon:icon
	         });

	         //Adding InfoWindow to the Marker.
	         (function (marker, data) {
	             google.maps.event.addListener(marker, "click", function (e) {
	                 $scope.InfoWindow.setContent("<div>" + data.doctor_name + ',' + data.location + ',' + data.dateandtime +"</div>");
	                 $scope.InfoWindow.open($scope.Map, marker);
	             });
	         })(marker, data);

	         //Plotting the Marker on the Map.
	         $scope.Latlngbounds.extend(marker.position);
	     }

	     //Adjusting the Map for best display.
	     $scope.Map.setCenter($scope.Latlngbounds.getCenter());
	     $scope.Map.fitBounds($scope.Latlngbounds);
		});
		
		});
	   }
	
	
	
	
	
	
	   //For med-supplier location in doctor dashboard 
	   $scope.getmedlocation = function(data){
		
		$http.get("./rest/doctordata/getmedical/"+data.med_id).then(function(response){
			$scope.getindividualmed = response.data;
			
			$scope.medaddress = $scope.getindividualmed[0].med_address; 
		   
		var geocoder = new google.maps.Geocoder();
		var address = $scope.medaddress;
	
		geocoder.geocode( { 'address': address}, function(results, status) {
	
		if (status == google.maps.GeocoderStatus.OK) {
			var latitude = results[0].geometry.location.lat();
			var longitude = results[0].geometry.location.lng();
	    } 
		
	
	     $scope.MapOptions = {
	         center: new google.maps.LatLng(9.925201, 78.119774),
	         zoom: 15,
	         mapTypeId: google.maps.MapTypeId.ROADMAP
	     };
	     
	     var icon = { 
	  		    url: './images/doctor.png'                             
	  	 };
	     
	     //Initializing the InfoWindow, Map and LatLngBounds objects.
	     $scope.InfoWindow = new google.maps.InfoWindow();
	     $scope.Latlngbounds = new google.maps.LatLngBounds();
	     $scope.Map = new google.maps.Map(document.getElementById("medMap"), $scope.MapOptions);
	
	     //Looping through the Array and adding Markers.
	     for (var i = 0; i < $scope.getindividualmed.length; i++) {
	         var data = $scope.getindividualmed[i];
	         var myLatlng = new google.maps.LatLng(latitude, longitude);
	
	         //Initializing the Marker object.
	         var marker = new google.maps.Marker({
	             position: myLatlng,
	             map: $scope.Map,
	             med_address: data.med_address,
	             icon:icon
	         });
	
	         //Adding InfoWindow to the Marker.
	         (function (marker, data) {
	             google.maps.event.addListener(marker, "click", function (e) {
	                 $scope.InfoWindow.setContent("<div>" + data.med_name + ',' + data.med_address + "</div>");
	                 $scope.InfoWindow.open($scope.Map, marker);
	             });
	         })(marker, data);
	
	         //Plotting the Marker on the Map.
	         $scope.Latlngbounds.extend(marker.position);
	     }
	
	     //Adjusting the Map for best display.
	     $scope.Map.setCenter($scope.Latlngbounds.getCenter());
	     $scope.Map.fitBounds($scope.Latlngbounds);
		});
		
		});
	   }
	
});