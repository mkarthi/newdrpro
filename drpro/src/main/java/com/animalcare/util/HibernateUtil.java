package com.animalcare.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;



public class HibernateUtil {

	
	private static SessionFactory sessionFactory = buildSessionFactory();
	 
    private static SessionFactory buildSessionFactory() {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
        	
        	Configuration configuration = new Configuration().configure(HibernateUtil.class.getResource("/hibernate.cfg.xml"));
        	ResourceBundle ap = ResourceBundle.getBundle("database");
        	configuration.setProperty("hibernate.connection.url", ap.getString("URL"));
        	configuration.setProperty("hibernate.connection.driver_class", ap.getString("DRIVER"));
        	configuration.setProperty("hibernate.connection.username", ap.getString("USER"));
        	configuration.setProperty("hibernate.connection.password", ap.getString("PWD"));
        	configuration.setProperty("hibernate.dialect", ap.getString("DIALECT"));
        	
        	StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
            serviceRegistryBuilder.applySettings(configuration.getProperties());
            ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        	
    		return sessionFactory;
        }  
        catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);  
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    
    public static void shutdown() {
    	// Close caches and connection pools
    	getSessionFactory().close();
    }
    
    public String clobToString(Clob data) {
	    StringBuilder sb = new StringBuilder();
	    try {
	        Reader reader = data.getCharacterStream();
	        BufferedReader br = new BufferedReader(reader);

	        String line;
	        while(null != (line = br.readLine())) {
	            sb.append(line);
	        }
	        br.close();
	    } catch (SQLException e) {
	        // handle this exception
	    } catch (IOException e) {
	        // handle this exception
	    }
	    return sb.toString();
	}
    
    public static String insertQuery(String query){
		Session session = null;
		try{
			session = getSessionFactory().openSession();
			Query queryobj = session.createSQLQuery(query);
			Integer insertedId = queryobj.executeUpdate();
			if(insertedId>0){
				return insertedId.toString();	
			}else{
				return null;	
			}			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			session.close();
		}
	}
    
    public static List<?> gteQueryList(String query){
		Session session = null;
		try{
			session = getSessionFactory().openSession();
			Query queryobj = session.createSQLQuery(query);
			List<?> al = queryobj.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
			System.out.println("Get query is:"+queryobj.getQueryString());
			if(al.size()>0){
				return al;	
			}else{
				return null;	
			}			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			session.close();
		}
	}
   
    
}
