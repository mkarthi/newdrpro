package com.animalcare.helper;


import com.animalcare.model.AnimalRegisration;
import com.animalcare.model.Appointment;
import com.animalcare.model.DoctorRegistration;
<<<<<<< HEAD
import com.animalcare.model.EventRegistration;
import com.animalcare.model.MedRegistration;
import com.animalcare.model.PetBoundaries;
import com.animalcare.model.Petimage;
import com.animalcare.model.UserRegistration;
import com.animalcare.service.RegistrationService;
import com.animalcare.vo.AppointmentVO;
import com.animalcare.vo.DoctorRegistrationVO;
import com.animalcare.vo.EventRegistrationVO;
import com.animalcare.vo.MedregRegistrationVO;
import com.animalcare.vo.PetBoundariesVO;
import com.animalcare.vo.UserRegistrationVO;
import com.sun.jersey.api.core.InjectParam;

public class RegistrationHelper {
  
	@InjectParam
	RegistrationService registerservice;	
	public int saveregistration(UserRegistrationVO userregistrationvo){
		int id=0;
		UserRegistration userregister = new UserRegistration();
			
		userregister.setCustomer_name(userregistrationvo.getCustomer_name());
		userregister.setCustomer_street(userregistrationvo.getCustomer_street());
		userregister.setCustomer_phone(userregistrationvo.getCustomer_phone());
		userregister.setCustomer_email(userregistrationvo.getCustomer_email());
		userregister.setPassword(userregistrationvo.getPassword());
		userregister.setUid(userregistrationvo.getUid());
		userregister.setTypeofreg(userregistrationvo.getTypeofreg());
		userregister.setUserphoto(userregistrationvo.getUserphoto());
		id = registerservice.saveregistration(userregister);
		
		return id;
	}
	
	public int savepetregistration(AnimalRegisration animalregvo){
		int id=0;
		AnimalRegisration animalregisration = new AnimalRegisration();
			
		animalregisration.setAnimal_category(animalregvo.getAnimal_category());
		animalregisration.setAnimal_name(animalregvo.getAnimal_name());
		animalregisration.setLatitude(animalregvo.getLatitude());
		animalregisration.setLongitude(animalregvo.getLongitude());
		animalregisration.setGender(animalregvo.getGender());
		animalregisration.setAge(animalregvo.getAge());
		animalregisration.setAddress(animalregvo.getAddress());
		animalregisration.setUid(animalregvo.getUid());
		animalregisration.setCustomer_id(animalregvo.getCustomer_id());
		id = registerservice.savepetregistration(animalregisration);
		
		return id;
	}
	
	public int savedocregistration(DoctorRegistrationVO doctorregistrationvo){
		int id=0;
		DoctorRegistration doctorregistrataion = new DoctorRegistration();
	    doctorregistrataion.setDoctor_name(doctorregistrationvo.getDoctor_name());
	    doctorregistrataion.setDoctor_email(doctorregistrationvo.getDoctor_email());
	    doctorregistrataion.setDoctor_phoneno(doctorregistrationvo.getDoctor_phoneno());
	    doctorregistrataion.setLocation(doctorregistrationvo.getLocation());
	    doctorregistrataion.setPassword(doctorregistrationvo.getPassword());
	    doctorregistrataion.setTypeofreg(doctorregistrationvo.getTypeofreg());
		id = registerservice.savedocregistration(doctorregistrataion);
		
		return id;
	}
	
	//save event registration  saveeventregistration
	public int saveeventregistration(EventRegistrationVO eventregistrationvo){
		int id=0;
		EventRegistration eventregistration = new EventRegistration();
		eventregistration.setEventname(eventregistrationvo.getEventname());
		eventregistration.setDate(eventregistrationvo.getDate());
		eventregistration.setVenue(eventregistrationvo.getVenue());
		eventregistration.setDescription(eventregistrationvo.getDescription());
		eventregistration.setContact(eventregistrationvo.getContact());
		eventregistration.setEventlogo(eventregistrationvo.getEventlogo());
		id =registerservice.saveeventregistration(eventregistration);
		return id;
	}
	
	
	
	
	
	//Medical supplier registration
	public int savemedregistration(MedregRegistrationVO medicalregvo){
		int id=0;
		MedRegistration medregistrataion = new MedRegistration();
		medregistrataion.setMed_name(medicalregvo.getMed_name());
		medregistrataion.setMed_email(medicalregvo.getMed_email());
	    medregistrataion.setMed_ph(medicalregvo.getMed_ph());
	    medregistrataion.setMed_address(medicalregvo.getMed_address());
	    medregistrataion.setMed_pwd(medicalregvo.getMed_pwd());
	    medregistrataion.setTypeofreg(medicalregvo.getTypeofreg());
		id = registerservice.savemedregistration(medregistrataion);
		
		return id;
	}
	
	//For doctor appointment
	public int saveappointment(AppointmentVO appointmentvo){
		int id=0;
		try{
			Appointment appointment = new Appointment();
		    appointment.setRequest(appointmentvo.getRequest());
			appointment.setDateandtime(appointmentvo.getDateandtime());
			appointment.setDoctorid(appointmentvo.getDoctorid());
			appointment.setCustomer_id(appointmentvo.getCustomer_id());
			appointment.setType(appointmentvo.getType());
			appointment.setPet_image(appointmentvo.getPet_image());
			appointment.setAnimalid(appointmentvo.getAnimalid());
			appointment.setMed_id(appointmentvo.getMed_id());
			appointment.setStatus("unseen");   
			/*appointment.setDocappointmentdate(appointmentvo.getDocappointmentdate());*/
			id = registerservice.saveappointment(appointment);
			
			String imgArray[] = appointmentvo.getPet_images().split(",");
			String videoArray[] = appointmentvo.getPet_videos().split(",");
			System.out.println("Pet Image:"+appointmentvo.getPet_images());
			for(int i=0;i<imgArray.length;i++){
				for(int k=0;k<videoArray.length;k++){
					Petimage petimage = new Petimage();
					petimage.setPet_images(imgArray[i]);
					petimage.setPet_videos(videoArray[k]);
					appointment.setAppointmentid(id);
					System.out.println("Pet Id:"+id);
					petimage.setAppointment(appointment);
					registerservice.savepetimage(petimage);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return id;
	}
	
	
	//For pet boundaries
	public int savepetboundary(PetBoundariesVO petboundariesvo){
		int id=0;
		PetBoundaries petboundaries = new PetBoundaries();
		petboundaries.setLatitude(petboundariesvo.getLatitude());
		petboundaries.setLongitude(petboundariesvo.getLongitude());
		petboundaries.setBoundary_radius(petboundariesvo.getBoundary_radius());
		petboundaries.setAddress(petboundariesvo.getAddress());
		id = registerservice.savepetboundary(petboundaries);
		return id;
	}
=======
import com.animalcare.model.MedRegistration;
import com.animalcare.model.Petimage;
import com.animalcare.model.UserRegistration;
import com.animalcare.service.RegistrationService;
import com.animalcare.vo.AppointmentVO;
import com.animalcare.vo.DoctorRegistrationVO;
import com.animalcare.vo.MedregRegistrationVO;
import com.animalcare.vo.UserRegistrationVO;
import com.sun.jersey.api.core.InjectParam;

public class RegistrationHelper {
  
	@InjectParam
	RegistrationService registerservice;	
	public int saveregistration(UserRegistrationVO userregistrationvo){
		int id=0;
		UserRegistration userregister = new UserRegistration();
			
		userregister.setCustomer_name(userregistrationvo.getCustomer_name());
		userregister.setCustomer_street(userregistrationvo.getCustomer_street());
		userregister.setCustomer_phone(userregistrationvo.getCustomer_phone());
		userregister.setCustomer_email(userregistrationvo.getCustomer_email());
		userregister.setPassword(userregistrationvo.getPassword());
		userregister.setUid(userregistrationvo.getUid());
		userregister.setTypeofreg(userregistrationvo.getTypeofreg());
		userregister.setUserphoto(userregistrationvo.getUserphoto());
		id = registerservice.saveregistration(userregister);
		
		return id;
	}
	
	public int savepetregistration(AnimalRegisration animalregvo){
		int id=0;
		AnimalRegisration animalregisration = new AnimalRegisration();
			
		animalregisration.setAnimal_category(animalregvo.getAnimal_category());
		animalregisration.setAnimal_name(animalregvo.getAnimal_name());
		animalregisration.setLatitude(animalregvo.getLatitude());
		animalregisration.setLongitude(animalregvo.getLongitude());
		animalregisration.setGender(animalregvo.getGender());
		animalregisration.setAge(animalregvo.getAge());
		animalregisration.setAddress(animalregvo.getAddress());
		animalregisration.setUid(animalregvo.getUid());
		animalregisration.setCustomer_id(animalregvo.getCustomer_id());
		id = registerservice.savepetregistration(animalregisration);
		
		return id;
	}
	
	public int savedocregistration(DoctorRegistrationVO doctorregistrationvo){
		int id=0;
		DoctorRegistration doctorregistrataion = new DoctorRegistration();
	    doctorregistrataion.setDoctor_name(doctorregistrationvo.getDoctor_name());
	    doctorregistrataion.setDoctor_email(doctorregistrationvo.getDoctor_email());
	    doctorregistrataion.setDoctor_phoneno(doctorregistrationvo.getDoctor_phoneno());
	    doctorregistrataion.setLocation(doctorregistrationvo.getLocation());
	    doctorregistrataion.setPassword(doctorregistrationvo.getPassword());
	    doctorregistrataion.setTypeofreg(doctorregistrationvo.getTypeofreg());
		id = registerservice.savedocregistration(doctorregistrataion);
		
		return id;
	}
	
	//Medical supplier registration
	public int savemedregistration(MedregRegistrationVO medicalregvo){
		int id=0;
		MedRegistration medregistrataion = new MedRegistration();
		medregistrataion.setMed_name(medicalregvo.getMed_name());
		medregistrataion.setMed_email(medicalregvo.getMed_email());
	    medregistrataion.setMed_ph(medicalregvo.getMed_ph());
	    medregistrataion.setMed_address(medicalregvo.getMed_address());
	    medregistrataion.setMed_pwd(medicalregvo.getMed_pwd());
	    medregistrataion.setTypeofreg(medicalregvo.getTypeofreg());
		id = registerservice.savemedregistration(medregistrataion);
		
		return id;
	}
	
	//For doctor appointment
	public int saveappointment(AppointmentVO appointmentvo){
		int id=0;
		try{
			Appointment appointment = new Appointment();
		    appointment.setRequest(appointmentvo.getRequest());
			appointment.setDateandtime(appointmentvo.getDateandtime());
			appointment.setDoctorid(appointmentvo.getDoctorid());
			appointment.setCustomer_id(appointmentvo.getCustomer_id());
			appointment.setType(appointmentvo.getType());
			appointment.setPet_image(appointmentvo.getPet_image());
			appointment.setAnimalid(appointmentvo.getAnimalid());
			appointment.setMed_id(appointmentvo.getMed_id());
			appointment.setStatus("unseen");   
			/*appointment.setDocappointmentdate(appointmentvo.getDocappointmentdate());*/
			id = registerservice.saveappointment(appointment);
			
			String imgArray[] = appointmentvo.getPet_images().split(",");
			String videoArray[] = appointmentvo.getPet_videos().split(",");
			System.out.println("Pet Image:"+appointmentvo.getPet_images());
			for(int i=0;i<imgArray.length;i++){
				for(int k=0;k<videoArray.length;k++){
					Petimage petimage = new Petimage();
					petimage.setPet_images(imgArray[i]);
					petimage.setPet_videos(videoArray[k]);
					appointment.setAppointmentid(id);
					System.out.println("Pet Id:"+id);
					petimage.setAppointment(appointment);
					registerservice.savepetimage(petimage);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return id;
	}
	
	
>>>>>>> refs/remotes/origin/master
}
