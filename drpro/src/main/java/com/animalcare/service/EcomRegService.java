package com.animalcare.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.animalcare.util.*;
import com.animalcare.model.EcomRegDetails;
import com.animalcare.model.EcomProductInsert;
import com.animalcare.model.EcomCattleInsert;



public class EcomRegService {
	
	public int ecomRegcontent(EcomRegDetails ecomregdetails){
			Session session = null;
			Transaction tx = null;
			int id =0;
			try{
				session = HibernateUtil.getSessionFactory().openSession();
				tx = session.beginTransaction();
				 id = (Integer) session.save(ecomregdetails);
				tx.commit();
			}catch(Exception e){
				e.printStackTrace();
				System.out.println("Ecomreg services services------------"+e);
					//return null;
			}
			finally{
				session.close();
			}
			
			return (int)id;
	}
	
	public int ecomProductInsert(EcomProductInsert ecomproductinsert){
		Session session = null;
		Transaction tx = null;
		int id =0;
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			 id = (Integer) session.save(ecomproductinsert);
			tx.commit();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("product services------------"+e);
				//return null;
		}
		finally{
			session.close();
		}
		
		return (int)id;
}        
	
	public int ecomCattleInsert(EcomCattleInsert ecomcattleinsert){
		Session session = null;
		Transaction tx = null;
		int id =0;
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			 id = (Integer) session.save(ecomcattleinsert);
			tx.commit();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("cattle services------------"+e);
				//return null;
		}
		finally{
			session.close();
		}
		
		return (int)id;
}
	
		public boolean update(String query){
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction transaction=session.beginTransaction();
		int id=0;
		try{
		id=session.createSQLQuery(query).executeUpdate();
		transaction.commit();
		}
		catch(Exception e){
			e.printStackTrace();
			transaction.rollback();
		}
		finally{
			session.close();
		}
		if(id>0)
			return true;
		else 
			return false;
	}
	
	
	public List<?> getQuery(String querystr){
		Session session = null;
		Transaction tx=null;
		List<?> result;
		
		try{
			session= HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			Query query 	= session.createSQLQuery(querystr);
			result	= query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
			tx.commit();
		}
		catch(Exception e){ 
			e.printStackTrace();
			return null;
			
		}finally{
			session.close();
 		}
		return result;
	}
	
}
