package com.animalcare.action;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;


import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.sun.jersey.core.header.ContentDisposition;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataParam;

@Path("/file")
public class FileUpload   {
	
    String tmppath;
    

	String joinPath;
	
	ResourceBundle bundle = ResourceBundle.getBundle("serversetup");
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	@POST
	@Path("/fileUri")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFileUploadedUri(@FormDataParam("drawobject") String drawobject){
		
	  
		List list =new ArrayList();
		Date date = new Date();
   	 	String dir = bundle.getString("serverpath");
   	 	String filename;
   	 	String tbPath="event/"+dateFormat.format(date);
   	 	File f=new File(dir+tbPath);
   	 	if(!f.exists())
   	 		f.mkdirs();
		
		try{
		    byte[] decodedBytes = DatatypeConverter.parseBase64Binary(drawobject);
	        BufferedImage bfi = ImageIO.read(new ByteArrayInputStream(decodedBytes));    
	    
	        date = new Date();
      	  	filename=Long.toString(date.getTime()+1);
      	  	String joinPath=tbPath + "/" +filename+".jpg";
      	  	HashMap<String, String> hm = new HashMap<String, String>();
			hm.put("path", ""+joinPath);
			hm.put("name", ""+filename+".jpg");
			list.add(hm);
	        File outputfile = new File(dir+joinPath);
	        ImageIO.write(bfi , "jpg", outputfile);
	        bfi.flush();
	        
	       
		}catch(Exception e)
        {  
			System.out.println(e);
        }
		
		return Response.status(200).entity(list).build();
	}
	
	@POST
	@Path("/upload/{dynfolder}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response uploadFile(@FormDataParam("files") List<FormDataBodyPart> files,@PathParam("dynfolder") String dynfolder)  {
		
		Date date = new Date();
   	 	String dir = bundle.getString("serverpath");
   	 	String filename;
   	 	String tbPath=dynfolder+"/"+dateFormat.format(date);
   	 	File f=new File(dir+tbPath);
   	 	
   	 	if(!f.exists())
   	 		f.mkdirs();

		long l=10;
		List list =new ArrayList();
		for (int i = 0; i < files.size(); i++) {
			FormDataBodyPart this_formDataBodyPartFile = files.get(i);
			ContentDisposition this_contentDispositionHeader = this_formDataBodyPartFile.getContentDisposition();
			InputStream this_fileInputStream = this_formDataBodyPartFile.getValueAs(InputStream.class);
			FormDataContentDisposition fileDetail=(FormDataContentDisposition) this_contentDispositionHeader;
			
			date = new Date();
      	  	filename=Long.toString(date.getTime()+(l++));
      	  	System.out.println("Filename:=="+filename);
      	  	System.out.println("FileDetail:=="+fileDetail);
      	  	String name =  fileDetail.getFileName();
      	  	System.out.println("Multiple img:---"+name);
      	  	String type= name.substring(name.lastIndexOf("."),name.length());
      	  	System.out.println("Type=="+type);
      	  	 joinPath=tbPath+"/"+filename+type;
      	  	
      	  	HashMap<String, String> hm = new HashMap<String, String>();
			hm.put("path", ""+joinPath);
			hm.put("name", ""+name);
			
			System.out.println("Joinpath--->"+joinPath);
			
						
      	  	writeToFile(this_fileInputStream, dir+joinPath);
			
			list.add(hm);
		};  
		
		return Response.status(200).entity(list).build();

	}
	
	//For Create Folder
	public static void createFolder(String bucketName, String folderName, AmazonS3 client) {
		// create meta-data for your folder and set content-length to 0
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(0);
		// create empty content
		InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
		// create a PutObjectRequest passing the folder name suffixed by /
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
				folderName + "/", emptyContent, metadata);
		System.out.println("putObjectRequest"+putObjectRequest);
		// send request to S3 to create folder
		client.putObject(putObjectRequest);
		System.out.println("client"+client);
	}
	
	
	
	// save uploaded file to new location
	private void writeToFile(InputStream uploadedInputStream,
		String uploadedFileLocation) {
		
		try {
			OutputStream out = new FileOutputStream(new File(
					uploadedFileLocation));
			int read = 0;
			byte[] bytes = new byte[1024];

			out = new FileOutputStream(new File(uploadedFileLocation));
			
			System.out.println("uploadedlocation"+uploadedFileLocation);
			System.out.println("uploadedInputStream"+uploadedInputStream);
			
			
			
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
				//System.out.println("Out"+out);
			}
			
			AWSCredentials credentials = new BasicAWSCredentials("AKIAIKXHKPET6WOBAEPA", "VtaLfuj+5btz90rd3gI2CQQ7O2vuYaMAeSVez6P9");
			AmazonS3 s3client = new AmazonS3Client(credentials);
			String karthi = null;
			String bucketName = "magazinebucket";
			String folderName = "Uploadimg" +"/"+ joinPath;    //newsimg/2019-01-10/1547131076946.jpg
			
			System.out.println("folderName"+folderName);
			
			//String fileName = folderName + "/" + joinPath;
			s3client.putObject(new PutObjectRequest(bucketName, folderName,new File("/var/www/html/newssiteimages/"+joinPath)));
		   
			System.out.println("client"+s3client);
			createFolder(bucketName,folderName, s3client);
			
			out.flush();
			out.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}
	
}