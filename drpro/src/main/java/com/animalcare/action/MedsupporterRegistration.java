package com.animalcare.action;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.animalcare.helper.RegistrationHelper;
import com.animalcare.service.RegistrationService;
import com.animalcare.vo.MedregRegistrationVO;
import com.sun.jersey.api.core.InjectParam;

@Path("/medsupport")
public class MedsupporterRegistration {
	
	@InjectParam
	RegistrationHelper registrationhelper;
	
	@InjectParam
	RegistrationService registrationservice;

	@POST
	@Path("/medregister")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveregistration(MedregRegistrationVO medicalregvo){
		int status= registrationhelper.savemedregistration(medicalregvo);
		if(status !=0)
			return Response.status(Status.OK).entity(Status.ACCEPTED).build();
		else
			return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
		
	}
	
	@GET
	@Path("/medreg/{typeofreg}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response gettypeofmedregistration(@PathParam("typeofreg") String typeofreg) {
		try{
			java.util.List<?> getregmed = registrationservice.getQuery("SELECT * FROM Medical_supplier where typeofreg='"+typeofreg+"'");
			if(getregmed.size()>0){
				return Response.status(Status.OK).entity(getregmed).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	@GET
	@Path("/getindividualmed/{med_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getindividualusermed(@PathParam("med_id") String med_id) {
		try{
			java.util.List<?> getindividualmed = registrationservice.getQuery("SELECT * FROM Medical_supplier where med_id='"+med_id+"'");
			if(getindividualmed.size()>0){
				return Response.status(Status.OK).entity(getindividualmed).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	//Getting doc list in med-supplier page
	@GET
	@Path("/getdoclist")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getdoctorlist() {
		try{
			java.util.List<?> getdoctormed = registrationservice.getQuery("SELECT * FROM vetdoctor_table");
			if(getdoctormed.size()>0){
				return Response.status(Status.OK).entity(getdoctormed).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	
	//Getting doc location in med-supplier page
	@GET
	@Path("/getdoclocation/{doctorid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getdoclocation(@PathParam("doctorid") String doctorid) {
		try{
			java.util.List<?> getdoclocation = registrationservice.getQuery("SELECT * FROM vetdoctor_table where doctorid='"+doctorid+"'");
			if(getdoclocation.size()>0){
				return Response.status(Status.OK).entity(getdoclocation).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	
	@GET
	@Path("/medappointnotification/{doctorid}/{status}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response medappointmentnotification(@PathParam("doctorid") String doctorid,@PathParam("status") String status) {   
		try{
			java.util.List<?> medappointmentnotify = registrationservice.getQuery("select a.dateandtime,a.appointmentid,a.status,b.med_name FROM appointment_table a,.Medical_supplier b"
					+ " where a.med_id=b.med_id and doctorid='"+doctorid+"' and status='"+status+"'");
			if(medappointmentnotify.size()>0){
				return Response.status(Status.OK).entity(medappointmentnotify).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
			}catch(Exception e){
				e.printStackTrace();
				return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
			}
	}
	
	
	
	@GET
	@Path("/medappointments/{doctorid}/{type}/{status}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response doctorappointment(@PathParam("doctorid") String doctorid,@PathParam("type") String type,@PathParam("status") String status) {   
		try{
			java.util.List<?> docappointment = registrationservice.getQuery("select a.dateandtime,a.appointmentid,a.status,b.med_name,b.med_address,b.med_ph,b.med_email,a.request FROM drpro.appointment_table a,drpro.Medical_supplier b"
					+ " where a.med_id=b.med_id and doctorid='"+doctorid+"' and type='"+type+"' and status='"+status+"' ");
			if(docappointment.size()>0){
				return Response.status(Status.OK).entity(docappointment).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
			}catch(Exception e){
				e.printStackTrace();
				return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
			}
	}
	
}
