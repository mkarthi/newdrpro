package com.animalcare.action;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

<<<<<<< HEAD
import com.animalcare.helper.RegistrationHelper;
import com.animalcare.model.Appointment;
import com.animalcare.model.UserRegistration;
import com.animalcare.service.RegistrationService;
import com.animalcare.util.HibernateUtil;
import com.animalcare.vo.AppointmentVO;
import com.animalcare.vo.DoctorRegistrationVO;
import com.animalcare.vo.EventRegistrationVO;
import com.animalcare.vo.UserRegistrationVO;
import com.sun.jersey.api.core.InjectParam;

@Path("/doctordata")
public class DoctorRegistration {
	
	
	@InjectParam
	RegistrationService registrationservice;
	
	@InjectParam
	RegistrationHelper registrationhepler;
	
	
	//Get doctor id for all page
	@GET
	@Path("/getdocid/{doctorid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getdocregistrationid(@PathParam("doctorid") String doctorid) {
		try{
			java.util.List<?> getregdocid = registrationservice.getQuery("SELECT * FROM vetdoctor_table where doctorid='"+doctorid+"'");
			if(getregdocid.size()>0){
				return Response.status(Status.OK).entity(getregdocid).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	//Get the animal list for chart and doctor view
	@GET
	@Path("/animallist")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getdocanimallist() {
		try{
			java.util.List<?> getanimallist = registrationservice.getQuery("SELECT * FROM animal");
			if(getanimallist.size()>0){
				return Response.status(Status.OK).entity(getanimallist).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	//save event registration
	@POST
	@Path("/events")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveeventregistration(EventRegistrationVO eventregistrationvo){
		int status= registrationhepler.saveeventregistration(eventregistrationvo);
		if(status !=0)
			return Response.status(Status.OK).entity(Status.ACCEPTED).build();
		else
			return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
	}
	
	@GET
	@Path("/eventlist")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getevent() {    
		try{
			java.util.List<?> geteventlist = registrationservice.getQuery("SELECT * from event_reg order by event_id desc");
			if(geteventlist.size()>0){
				return Response.status(Status.OK).entity(geteventlist).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
=======
import com.animalcare.model.Appointment;
import com.animalcare.model.UserRegistration;
import com.animalcare.service.RegistrationService;
import com.animalcare.util.HibernateUtil;
import com.animalcare.vo.AppointmentVO;
import com.animalcare.vo.DoctorRegistrationVO;
import com.animalcare.vo.UserRegistrationVO;
import com.sun.jersey.api.core.InjectParam;

@Path("/doctordata")
public class DoctorRegistration {
	
	
	@InjectParam
	RegistrationService registrationservice;
	
	
	
	//Get doctor id for all page
	@GET
	@Path("/getdocid/{doctorid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getdocregistrationid(@PathParam("doctorid") String doctorid) {
		try{
			java.util.List<?> getregdocid = registrationservice.getQuery("SELECT * FROM vetdoctor_table where doctorid='"+doctorid+"'");
			if(getregdocid.size()>0){
				return Response.status(Status.OK).entity(getregdocid).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	//Get the animal list for chart and doctor view
	@GET
	@Path("/animallist")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getdocanimallist() {
		try{
			java.util.List<?> getanimallist = registrationservice.getQuery("SELECT * FROM animal");
			if(getanimallist.size()>0){
				return Response.status(Status.OK).entity(getanimallist).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
>>>>>>> refs/remotes/origin/master
	
	
	@GET
	@Path("/doctorpetchart")
	@Produces(MediaType.APPLICATION_JSON)
	public Response doctorpetchart() {    //SELECT animal_category AS x, COUNT(*) AS `value` FROM drpro.animal GROUP BY animal_category--->get total pet
		try{
			java.util.List<?> getanimalchart = registrationservice.getQuery("SELECT animal_category AS x,COUNT(*) AS value FROM animal GROUP BY animal.animal_category");
			if(getanimalchart.size()>0){
				return Response.status(Status.OK).entity(getanimalchart).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	@GET
	@Path("/customerview")
	@Produces(MediaType.APPLICATION_JSON)
	public Response doctorcustomerview() {    //SELECT animal_category AS x, COUNT(*) AS `value` FROM drpro.animal GROUP BY animal_category--->get total pet
		try{
			java.util.List<?> customerview = registrationservice.getQuery("SELECT * from customer_table");
			if(customerview.size()>0){
				return Response.status(Status.OK).entity(customerview).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	// get the individual customer location 
	@GET
	@Path("/customerlocation/{customer_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response doctorcustomerlocation(@PathParam("customer_id") String customer_id) {   
		try{
			java.util.List<?> customerlocation = registrationservice.getQuery("SELECT * from customer_table where customer_id='"+customer_id+"'");
			if(customerlocation.size()>0){
				return Response.status(Status.OK).entity(customerlocation).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	// For getting the all medical supplier details and location
	@GET
	@Path("/getmedical")
	@Produces(MediaType.APPLICATION_JSON)
	public Response doctorgetmedical() {   
		try{
			java.util.List<?> getmedical = registrationservice.getQuery("SELECT * from Medical_supplier");
			if(getmedical.size()>0){
				return Response.status(Status.OK).entity(getmedical).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	@GET
	@Path("/getmedical/{med_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response doctorgetindividualmedical(@PathParam("med_id") String med_id) {   
		try{
			java.util.List<?> getmedicalid = registrationservice.getQuery("SELECT * from Medical_supplier where med_id='"+med_id+"'");
			if(getmedicalid.size()>0){
				return Response.status(Status.OK).entity(getmedicalid).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
		}
	}
	
	
	//For creating appointment
	@GET
	@Path("/appointments/{doctorid}/{type}/{status}/{request}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response doctorappointment(@PathParam("doctorid") String doctorid,@PathParam("type") String type,@PathParam("status") String status,@PathParam("request") String request) {   
		try{
			java.util.List<?> docappointment = registrationservice.getQuery("select a.animalid,a.animal_name,a.animal_category,a.address,a.age,a.uid,b.dateandtime,b.doctorid,b.appointmentid,b.request,c.customer_name,c.customer_email,c.customer_phone,b.status FROM animal a,appointment_table b,customer_table c"
					+ " where a.animalid=b.animalid and a.customer_id=c.customer_id and doctorid='"+doctorid+"' and type='"+type+"' and status='"+status+"' and request='"+request+"' order by appointmentid desc");
			if(docappointment.size()>0){
				return Response.status(Status.OK).entity(docappointment).build();
			}
			else{
				return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
			}
			}catch(Exception e){
				e.printStackTrace();
				return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
			}
	}
	
	
	//For doctor side to doctor for appointment
	@POST
	@Path("/doctorappointment")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response userappoint(AppointmentVO appointmentvo){
		Session session = null;
		Transaction tx=null;
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();	
			int result = 0;
			Query query = null;
			
			String docdateandtime = appointmentvo.getDocappointmentdate();
			//String dateandtime = appointmentvo.getDateandtime();
			String request = appointmentvo.getRequest();
			String customerstatus = appointmentvo.getCustomerstatus();
			
			int appointmentid = appointmentvo.getAppointmentid();
				
			String hql="update Appointment set docappointmentdate=:docappointmentdate,request=:request,customerstatus=:customerstatus where appointmentid=:appointmentid";
				
			query = session.createQuery(hql);
				
			query.setParameter("docappointmentdate", docdateandtime);
			//query.setParameter("dateandtime", dateandtime);
			query.setParameter("request", request);
			query.setParameter("customerstatus", customerstatus);
			query.setParameter("appointmentid", appointmentid);
				
			System.out.println("appointmentid view:"+appointmentid);
			result = query.executeUpdate();
			System.out.println("appointment update value"+result);
			tx.commit();
			if(result>0)
				return Response.status(Status.OK).entity(Status.ACCEPTED).build();
			else 
				return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
		
		
		//Doctor Edit Profile
		@POST
		@Path("/doctorupdate")
		@Consumes(MediaType.APPLICATION_JSON)
	 	@Produces(MediaType.APPLICATION_JSON)
		public Response doctorupdate(DoctorRegistrationVO doctorregvo){
			Session session = null;
			Transaction tx=null;
			try{
				session = HibernateUtil.getSessionFactory().openSession();
				tx = session.beginTransaction();	
				int result = 0;
				Query query = null;
				
				DoctorRegistration doctorreg = new DoctorRegistration();
				
				String doctorname = doctorregvo.getDoctor_name();
				String phoneno = doctorregvo.getDoctor_phoneno();
				String address = doctorregvo.getLocation();
				String dateandtime = doctorregvo.getDateandtime();
				String doctor_status = doctorregvo.getDoctor_status();
				/*String photo = userregistrationvo.getUserphoto();*/
				
				int doctorid = doctorregvo.getDoctorid();
				
				String hql="update DoctorRegistration set doctor_name=:doctor_name,doctor_phoneno=:doctor_phoneno,location=:location,dateandtime=:dateandtime,doctor_status=:doctor_status where doctorid=:doctorid";
				
				query = session.createQuery(hql);
				query.setParameter("doctor_name", doctorname);
				query.setParameter("doctor_phoneno", phoneno);
				query.setParameter("location", address );
				query.setParameter("dateandtime", dateandtime);
				query.setParameter("doctor_status", doctor_status);
				
				query.setParameter("doctorid", doctorid);
				
				System.out.println("Doctorid view:"+doctorid);
				result = query.executeUpdate();
				System.out.println("Doctor update value"+result);
				tx.commit();
				if(result>0)
					return Response.status(Status.OK).entity(Status.ACCEPTED).build();
				else 
					return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
			}catch(Exception e){
				e.printStackTrace();
				return null;
			}
			
		}	
		
		
		//For appointment's customer chart
		@GET
		@Path("/appointmentpetchart/{doctorid}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response appointmentpetchart(@PathParam("doctorid") String doctorid) {   
			try{
				java.util.List<?> appointmentchart = registrationservice.getQuery("select a.animalid,a.disease_range,a.animal_category as x,count(animal_category) as value,animal_name,c.customer_name from drpro.animal a,drpro.appointment_table b,drpro.customer_table c"
						+ " where a.animalid=b.animalid and a.customer_id=b.customer_id and b.customer_id=c.customer_id and b.doctorid='"+doctorid+"' group by animal_category,animalid,animal_name,customer_name,disease_range");
				if(appointmentchart.size()>0){
					return Response.status(Status.OK).entity(appointmentchart).build();
				}
				else{
					return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
				}
				}catch(Exception e){
					e.printStackTrace();
					return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
				}
		}
		
		//For Bell Notification view and count
		@GET
		@Path("/appointnotification/{doctorid}/{status}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response appointmentnotification(@PathParam("doctorid") String doctorid,@PathParam("status") String status) {   
			try{
				java.util.List<?> appointmentnotify = registrationservice.getQuery("select a.animalid,a.animal_category,b.dateandtime,b.appointmentid,b.status,c.customer_name,c.userphoto FROM animal a,appointment_table b,"
						+ "customer_table c where a.animalid=b.animalid and a.customer_id=c.customer_id and doctorid='"+doctorid+"' and status='"+status+"' order by appointmentid desc");
				if(appointmentnotify.size()>0){
					return Response.status(Status.OK).entity(appointmentnotify).build();
				}
				else{
					return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
				}
				}catch(Exception e){
					e.printStackTrace();
					return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
				}
		}
		
		
		//update notificaiton status
		@POST
		@Path("/statusupdate")
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response notificationupdate(AppointmentVO appointmentvo){
			Session session = null;
			Transaction tx=null;
			try{
				session = HibernateUtil.getSessionFactory().openSession();
				tx = session.beginTransaction();	
				int result = 0;
				Query query = null;
						
				Appointment appointment = new Appointment();
						
				String status = appointmentvo.getStatus();
						
				int appointmentid = appointmentvo.getAppointmentid();
						
				String hql="update Appointment set status=:status where appointmentid=:appointmentid";
						
				query = session.createQuery(hql);
				query.setParameter("status", status);
						
				query.setParameter("appointmentid", appointmentid);
						
				System.out.println("Appointment view:"+appointmentid);
				result = query.executeUpdate();
				System.out.println("Appointment update value"+result);
				tx.commit();
				if(result>0)
						return Response.status(Status.OK).entity(Status.ACCEPTED).build();
					else 
						return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
				}catch(Exception e){
					e.printStackTrace();
					return null;
				}
					
			}	
		
		
		//Doctor confirm notification to customer
		@GET
		@Path("/confirmedusernotification/{customer_id}/{request}/{customerstatus}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response confirmednotification(@PathParam("customer_id") String customer_id,@PathParam("request") String request,@PathParam("customerstatus") String customerstatus) {   
			try{
				java.util.List<?> confirmationmentnotify = registrationservice.getQuery("select a.doctor_name,a.doctor_email,a.doctor_phoneno,b.appointmentid,b.docappointmentdate,b.request,c.animalid,c.animal_name,c.animal_category from "
						+ "vetdoctor_table a,appointment_table b,animal c where a.doctorid=b.doctorid and b.animalid=c.animalid and b.customer_id='"+customer_id+"' and request='"+request+"' and customerstatus='"+customerstatus+"' order by appointmentid desc");
				if(confirmationmentnotify.size()>0){
					return Response.status(Status.OK).entity(confirmationmentnotify).build();
				}
				else{
					return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
				}
				}catch(Exception e){
					e.printStackTrace();
					return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
				}
		}
		
		
		//update notificaiton status
		@POST
		@Path("/confirmationupdate")
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response confirmationupdate(AppointmentVO appointmentvo){
				Session session = null;
				Transaction tx=null;
				try{
					session = HibernateUtil.getSessionFactory().openSession();
					tx = session.beginTransaction();	
					int result = 0;
					Query query = null;
							
					Appointment appointment = new Appointment();
							
					String customerstatus = appointmentvo.getCustomerstatus();
							
					int appointmentid = appointmentvo.getAppointmentid();
								
					String hql="update Appointment set customerstatus=:customerstatus where appointmentid=:appointmentid";
								
					query = session.createQuery(hql);
					query.setParameter("customerstatus", customerstatus);
								
					query.setParameter("appointmentid", appointmentid);
								
					System.out.println("Appointment view:"+appointmentid);
					result = query.executeUpdate();
					System.out.println("Appointment update value"+result);
					tx.commit();
					if(result>0)
							return Response.status(Status.OK).entity(Status.ACCEPTED).build();
						else 
							return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
					}catch(Exception e){
						e.printStackTrace();
						return null;
					}
							
			}	
		
		
		//Confirmation view in customer notificaiton
		@GET
		@Path("/shownnotification/{customer_id}/{customerstatus}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response shownnotification(@PathParam("customer_id") String customer_id,@PathParam("customerstatus") String customerstatus) {   
			try{
				java.util.List<?> shownnotify = registrationservice.getQuery("select a.doctor_name,a.doctor_email,a.doctor_phoneno,b.appointmentid,b.docappointmentdate,b.request,b.customerstatus,c.animalid,c.animal_name,c.animal_category from "
						+ "vetdoctor_table a,appointment_table b,animal c where a.doctorid=b.doctorid and b.animalid=c.animalid and b.customer_id='"+customer_id+"' and customerstatus='"+customerstatus+"' order by appointmentid desc");
				if(shownnotify.size()>0){
					return Response.status(Status.OK).entity(shownnotify).build();
				}
				else{
					return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
				}
				}catch(Exception e){
					e.printStackTrace();
					return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
				}
		}
		
		
		@GET
		@Path("/getallimages/{appointmentid}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getallimages(@PathParam("appointmentid") String appointmentid) {   
			try{
				java.util.List<?> getimgs = registrationservice.getQuery("select * from petimage where appointmentid='"+appointmentid+"'");
				if(getimgs.size()>0){
					return Response.status(Status.OK).entity(getimgs).build();
				}
				else{
					return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
				}
				}catch(Exception e){
					e.printStackTrace();
					return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
				}
		}
		
		@GET
		@Path("/getapprovedList/{doctorid}/{type}/{request}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getapprovedcustomerList(@PathParam("doctorid") String doctorid,@PathParam("type") String type,@PathParam("request") String request) {   
			try{
				java.util.List<?> getconfirmedlist = registrationservice.getQuery("select a.animalid,a.animal_name,a.animal_category,a.address,a.age,a.uid,b.doctorid,b.appointmentid,b.request,c.customer_name,c.customer_email,c.customer_phone,b.status,"
						+ "b.dateandtime,b.docappointmentdate FROM drpro.animal a,drpro.appointment_table b,drpro.customer_table c "
						+ "where a.animalid=b.animalid and a.customer_id=c.customer_id and doctorid='"+doctorid+"' and type='"+type+"' and request='"+request+"' order by appointmentid desc");
				if(getconfirmedlist.size()>0){
					return Response.status(Status.OK).entity(getconfirmedlist).build();
				}
				else{
					return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
				}
				}catch(Exception e){
					e.printStackTrace();
					return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
				}
		}
		
		
		//Getting med approved list form doctor dashboard
		@GET
		@Path("/getmedapprovedList/{doctorid}/{type}/{request}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getapprovedmedList(@PathParam("doctorid") String doctorid,@PathParam("type") String type,@PathParam("request") String request) {   
			try{
				java.util.List<?> getmedconfirmedlist = registrationservice.getQuery("select a.dateandtime,a.docappointmentdate,a.appointmentid,a.status,b.med_name FROM drpro.appointment_table a,drpro.Medical_supplier b where a.med_id=b.med_id and doctorid='"+doctorid+"' and"
						+ " type='"+type+"' and request='"+request+"'");
				if(getmedconfirmedlist.size()>0){
					return Response.status(Status.OK).entity(getmedconfirmedlist).build();
				}
				else{
					return Response.status(Status.OK).entity(Status.INTERNAL_SERVER_ERROR).build();
				}
				}catch(Exception e){
					e.printStackTrace();
					return Response.status(Status.OK).entity(Status.PRECONDITION_FAILED).build();
				}
		}
		
		
		//update notificaiton status
		@POST
		@Path("/updateapproveddate")
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response updateappointmentdate(AppointmentVO appointmentvo){
			Session session = null;
			Transaction tx=null;
			try{
				session = HibernateUtil.getSessionFactory().openSession();
				tx = session.beginTransaction();	
				int result = 0;
				Query query = null;
									
				Appointment appointment = new Appointment();
									
				String docappointmentdate = appointmentvo.getDocappointmentdate();
									
				int appointmentid = appointmentvo.getAppointmentid();
										
				String hql="update Appointment set docappointmentdate=:docappointmentdate where appointmentid=:appointmentid";
										
				query = session.createQuery(hql);
				query.setParameter("docappointmentdate", docappointmentdate);
										
				query.setParameter("appointmentid", appointmentid);
										
				System.out.println("Appointment view:"+appointmentid);
				result = query.executeUpdate();
				System.out.println("Appointment update value"+result);
				tx.commit();
					if(result>0)
						return Response.status(Status.OK).entity(Status.ACCEPTED).build();
					else 
						return Response.status(Status.OK).entity(Status.NOT_ACCEPTABLE).build();
			 }catch(Exception e){
					e.printStackTrace();
					return null;
			 }
									
		}	
		
}
