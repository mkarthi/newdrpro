package com.animalcare.vo;

public class AnimalRegistrationVO {

	private int animalid;
	private String animal_category; 
	private String latitude;
	private String longitude;
	private String customer_id;
	private String animal_name;
	private String age;
	private String gender;
	private String address;
	private String uid;
	
	public int getAnimalid() {
		return animalid;
	}
	public void setAnimalid(int animalid) {
		this.animalid = animalid;
	}
	public String getAnimal_category() {
		return animal_category;
	}
	public void setAnimal_category(String animal_category) {
		this.animal_category = animal_category;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getAnimal_name() {
		return animal_name;
	}
	public void setAnimal_name(String animal_name) {
		this.animal_name = animal_name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	
}
