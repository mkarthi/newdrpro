package com.animalcare.vo;

public class PetBoundariesVO {
	
	private int pet_boundariesid;
	private String latitude;
	private String longitude;
	private String boundary_radius;
	private String address;
	
	public int getPet_boundariesid() {
		return pet_boundariesid;
	}
	public void setPet_boundariesid(int pet_boundariesid) {
		this.pet_boundariesid = pet_boundariesid;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getBoundary_radius() {
		return boundary_radius;
	}
	public void setBoundary_radius(String boundary_radius) {
		this.boundary_radius = boundary_radius;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	
}
