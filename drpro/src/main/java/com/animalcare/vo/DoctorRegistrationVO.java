package com.animalcare.vo;

public class DoctorRegistrationVO {

	private int doctorid; 
	private String doctor_name;
	private String doctor_email;
	private String doctor_phoneno;
	private String password;
	private String location;
	private String typeofreg;
	private String dateandtime;
	private String doctor_status;
	
	public int getDoctorid() {
		return doctorid;
	}
	public void setDoctorid(int doctorid) {
		this.doctorid = doctorid;
	}
	public String getDoctor_name() {
		return doctor_name;
	}
	public void setDoctor_name(String doctor_name) {
		this.doctor_name = doctor_name;
	}
	public String getDoctor_email() {
		return doctor_email;
	}
	public void setDoctor_email(String doctor_email) {
		this.doctor_email = doctor_email;
	}
	public String getDoctor_phoneno() {
		return doctor_phoneno;
	}
	public void setDoctor_phoneno(String doctor_phoneno) {
		this.doctor_phoneno = doctor_phoneno;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getTypeofreg() {
		return typeofreg;
	}
	public void setTypeofreg(String typeofreg) {
		this.typeofreg = typeofreg;
	}
	public String getDateandtime() {
		return dateandtime;
	}
	public void setDateandtime(String dateandtime) {
		this.dateandtime = dateandtime;
	}
	public String getDoctor_status() {
		return doctor_status;
	}
	public void setDoctor_status(String doctor_status) {
		this.doctor_status = doctor_status;
	}
    	
	
}
